# Revision history for primeTests

## 0.3.0.0 -- 2018-11-20

* Add Miller-Rabin Test

## 0.2.0.0 -- 2018-11-20

* Add Solovay-Strassen Test

## 0.1.1.0 -- 2018-11-17

* Add Legendre symbol
* Add Jacobi symbol

## 0.1.0.0 -- 2018-11-14

* First version. Released on an unsuspecting world.
