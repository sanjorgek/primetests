# primeTests

Primality tests

[![primeTests](https://img.shields.io/badge/primeTests-v0.3.0.0-blue.svg?style=plastic)](#)
[![pipeline status](https://gitlab.com/sanjorgek/primetests/badges/master/pipeline.svg)](https://gitlab.com/sanjorgek/primetests/commits/master)

## Description

### Symbols

Includes the [Legendre](https://en.wikipedia.org/wiki/Legendre_symbol) and
[Jacobi](https://en.wikipedia.org/wiki/Jacobi_symbol) symbols and his porperties
with tests.

### Tests

Includes the [Solovay-Strassen](https://en.wikipedia.org/wiki/Solovay–Strassen_primality_test)
and [Miller-Rabin](https://en.wikipedia.org/wiki/Miller–Rabin_primality_test) test.
