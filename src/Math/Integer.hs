{-# OPTIONS_GHC -fno-warn-tabs #-}
{-# OPTIONS_HADDOCK show-extensions #-}
{-|
Module      : Integer
Description : Functions over integers
Copyright   : (c) Jorge Santiago Alvarez Cuadros, 2018
License     : GPL-3
Maintainer  : sanjorgek@ciencias.unam.mx
Stability   : experimental
Portability : portable

Auxiliar functions over integers
-}
module Math.Integer (
  powMod
  , squareMod
  , isCoprime
  , find2km
) where

{-|
Square module

>>> squareMod 3 2
1
>>> squareMod 3 8
1
>>> squareMod 2 7
4
-}
squareMod :: Integral a => a -> a -> a
squareMod a b = (b*b) `rem` a

mulMod :: Integral a => a -> a -> a -> a
mulMod a b c = (b*c) `mod` a

pow1 :: (Num a, Integral b) => (a -> a -> a) -> (a -> a) -> a -> b -> a
pow1 _ _ _ 0 = 1
pow1 mul sq x1 n1 = f x1 n1 1
  where
    f x n y
      | n == 1 = x `mul` y
      | r == 0 = f x2 q y
      | otherwise = f x2 q (x `mul` y)
      where
         (q,r) = quotRem n 2
         x2 = sq x

{-|
Power module m. The first element is the module.

>>> powMod 10 2 5
2
>>> powMod 10 2 4
6
-}
powMod :: Integral a => a -> a -> a -> a
powMod m = pow1 (mulMod m) (squareMod m)

{-|
>>> isCoprime 2 3
True
-}
isCoprime :: Integral a => a -> a -> Bool
isCoprime a b = gcd a b == 1

{-|
For all n shows k and m that `n=(2^k)m`

>>> find2km 8
(3,1)
>>> find2km 9
(0,9)
>>> find2km 10
(1,5)
>>> find2km 11
(0,11)
>>> find2km 12
-}
find2km :: Integral a => a -> (a,a)
find2km = f 0
  where
    f k m
      | r == 1 = (k,m)
      | otherwise = f (k+1) q
      where (q,r) = quotRem m 2
