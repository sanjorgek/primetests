{-# OPTIONS_GHC -fno-warn-tabs #-}
{-# OPTIONS_HADDOCK show-extensions #-}
{-|
Module      : Symbol
Description : Relations between integers
Copyright   : (c) Jorge Santiago Alvarez Cuadros, 2018
License     : GPL-3
Maintainer  : sanjorgek@ciencias.unam.mx
Stability   : experimental
Portability : portable

[Legendre symbol](https://en.wikipedia.org/wiki/Legendre_symbol) and [Jacobi symbol](https://en.wikipedia.org/wiki/Jacobi_symbol)
-}
module Math.Integer.Symbol (
  legendre
  , jacobi
) where

import Math.Integer

{-|
The legendre symbol is calculeted with his original definition

(a/p) === a^(div (p-1) 2) mod p and (a/p) in [-1,0,1]
-}
legendre:: Integer -> Integer -> Integer
legendre 1 _ = 1
legendre a p = let
    m = powMod p a (div (p-1) 2)
  in if (m==0) || (m==1)
    then m
    else m-p

{-|
The jacobi symbol is calculated from his properties
-}
jacobi:: Integer -> Integer -> Integer
jacobi _ 1 = 1
jacobi 0 _ = 0
jacobi 1 n
  | odd n = 1
  | otherwise = 0
jacobi 2 n
  | isCoprime 2 n && ((n `mod` 8 == 3) || (n `mod` 8 == 5)) = -1
  | isCoprime 2 n && ((n `mod` 8 == 1) || (n `mod` 8 == 7)) = 1
  | otherwise = 0
jacobi (-1) n
  | odd n && n `mod` 4 == 3 = -1
  | odd n && n `mod` 4 == 1 = 1
  | otherwise = 0
jacobi a n
  | odd a && odd n && a < 0 = jacobi ((-1)*a) n * jacobi (-1) n
  | odd n && a `mod` 2 == 0 && isCoprime a n = jacobi 2 n * jacobi (div a 2) n
  | odd a && odd n && not (isCoprime a n) = 0
  | odd a && odd n && a > n = jacobi (a `mod` n) n
  | odd a && odd n && isCoprime a n = ((-1)^div ((a-1)*(n-1)) 4)*jacobi n a
  | otherwise = 0
