{-# OPTIONS_GHC -fno-warn-tabs #-}
{-# OPTIONS_HADDOCK show-extensions #-}
{-|
Module      : Primality
Description : Test primality on integers
Copyright   : (c) Jorge Santiago Alvarez Cuadros, 2018
License     : GPL-3
Maintainer  : sanjorgek@ciencias.unam.mx
Stability   : experimental
Portability : portable

Test primality on integers.
-}
module Math.Test.Primality (
  solovayStrassen
  , millerRabin
) where

import           System.Random
import           Math.Integer.Symbol
import           Math.Integer

{-|
The historic [Solovay-Strassen](https://en.wikipedia.org/wiki/Solovay–Strassen_primality_test)
primality test

>>> import System.Random
>>> g <- newStdGen
>>> solovayStrassen g 13 13
True
-}
solovayStrassen :: StdGen -> Integer -> Integer -> Bool
solovayStrassen _ _ 0 = True
solovayStrassen g n k = do
  let (a,g2) = randomR (2,n-1) g
  let x = jacobi a n
  let y = legendre a n
  x/=0 && y == x && solovayStrassen g2 n (k-1)

{-|
[Miller-Rabin](https://en.wikipedia.org/wiki/Miller–Rabin_primality_test) primality
test

>>> import System.Random
>>> g <- newStdGen
>>> millerRabin g 293 50
True
-}
millerRabin :: StdGen -> Integer -> Integer -> Bool
millerRabin _ _ 0 = True
millerRabin g n k = do
  let (a,g2) = randomR (2,n-2) g
  millerRabinPrimality n a && millerRabin g2 n (k-1)

millerRabinPrimality :: Integer -> Integer -> Bool
millerRabinPrimality n a
  | n < 2 = False
  | even n = False
  | b0 == 1 || b0 == n1 = True
  | otherwise = iter (tail b)
  where
    n1 = n-1
    (k,m) = find2km n1
    b0 = powMod n a m
    b = take (fromIntegral k) $ iterate (squareMod n) b0
    iter [] = False
    iter (x:xs)
      | x == 1 = False
      | x == n1 = True
      | otherwise = iter xs
