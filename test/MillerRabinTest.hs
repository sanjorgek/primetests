{-# OPTIONS_GHC -fno-warn-tabs #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Main where

import           Math.Test.Primality
import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck
import           Control.Monad
import           System.Random
import           Prime

millerRabinTest = describe "Miller-Rabin prime test" $
  context "test primality" $ do
    prop "with some well know primes" $
      \k -> do
        let k' = abs k
        g <- newStdGen
        p <- pick primes
        unless (millerRabin g p k') $ error "no se"
    prop "false positive" $
      \a b -> do
        g <- newStdGen
        let p = primes !! (abs a + 1 `mod` length primes)
        let q = primes !! (abs b + 1 `mod` length primes)
        unless (millerRabin g (p*q) 0) $ error "no se"
    prop "no positive" $
      \a -> do
        g <- newStdGen
        let p = primes !! (abs a + 1 `mod` length primes)
        when (millerRabin g (p*p) p) $ error "no se"
    prop "even no positive" $
      \a b -> do
        g <- newStdGen
        let p = primes !! (abs a + 1 `mod` length primes)
        let q = primes !! (abs b + 1 `mod` length primes)
        when (millerRabin g (p*(2^q)) p) $ error "no se"

main::IO ()
main = hspec $
  describe "Math.Integer.hs" millerRabinTest
