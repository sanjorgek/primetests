{-# OPTIONS_GHC -fno-warn-tabs #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Main where

import           Math.Integer.Symbol
import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck
import           Control.Monad
import           System.Random
import           Data.Functor
import           Prime

legendreTest = describe "Legendre symbol" $
  context "have" $ do
    it "(7/9907)=-1" $
      legendre 7 9907 `shouldBe` (-1)
    it "(11/9907)=1" $
      legendre 11 9907 `shouldBe` 1
    it "(13/9907)=1" $
      legendre 13 9907 `shouldBe` 1
    it "(19/45)=1" $
      legendre 19 45 `shouldBe` 1
    prop "(a/p) in {-1,0,1}" $
      \a -> do
        p <- pick primes
        let l = legendre a p
        let b = l<=1 && l>=(-1)
        unless b $ error "no se"
    prop "(2/p)=(-1)^((p^2-1)/8) if p == 1 or 7 `mod` 8 or p == 3 or 5 `mod` 8" $
      \a -> let
          p = primes !! (abs a `mod` length primes)
          l = legendre 1 p
        in (p /= 3 `mod` 8) || (p /= 1 `mod` 8) || (p /= 5 `mod` 8) || (p /= 7 `mod` 8) || (l==((-1) ^ div ((p^2)-1) 2))
    prop "(2/p)=1 if p == 1 or 7 `mod` 8" $
      \a -> let
          p = primes !! (abs a `mod` length primes)
          l = legendre 1 p
        in (p /= 1 `mod` 8) || (p /= 7 `mod` 8) || (l==1)
    prop "(2/p)=(-1) if p == 3 or 5 `mod` 8" $
      \a -> let
          p = primes !! (abs a `mod` length primes)
          l = legendre 1 p
        in (p /= 3 `mod` 8) || (p /= 5 `mod` 8) || (l==(-1))
    prop "If a = b  (mod p), then (a/p) = (b/p)" $
      \a b -> do
        p <- pick primes
        let l = (a /= b `mod` p) || (legendre a p == legendre b p)
        unless l $ error "no se"
    prop "(ab/n)=(a/n)(b/n)" $
      \a b -> do
        let b' = abs b
        p <- pick primes
        let l = legendre (a*b') p == legendre a p * legendre b' p
        unless l $ error "no se"
    prop "(a^2/p) in {0,1}" $
      \a -> do
        let a' = abs a
        p <- pick primes
        let l = legendre (a'*a') p
        let b = l==0 || l==1
        unless b $ error "no se"
    prop "(-1/p)=(-1)^((p-1)/2) if p == 3 `mod` 4 or p == 1 `mod` 4" $
      \a -> let
          p = primes !! (abs a `mod` length primes)
          l = legendre 1 p
        in (p /= 3 `mod` 4) || (p /= 1 `mod` 4) || (l==((-1) ^ div (p-1) 2))
    prop "(-1/p)=1 if p == 1 `mod` 4" $
      \a -> let
          p = primes !! (abs a `mod` length primes)
          l = legendre 1 p
        in (p /= 1 `mod` 4) || (l==1)
    prop "(-1/p)=(-1) if p == 3 `mod` 4" $
      \a -> let
          p = primes !! (abs a + 1 `mod` length primes)
          l = legendre 1 p
        in (p /= 3 `mod` 4) || (l==(-1))
    prop "if p/=q then (q/p)(p/q)=(-1)^(div (p-1) 2 * div (q-1) 2)" $
      \a b -> let
        p = primes !! (abs a + 1 `mod` length primes)
        q = primes !! (abs b + 1 `mod` length primes)
        l1 = legendre p q
        l2 = legendre q p
      in (q==p) || (l1*l2==(-1)^(div (p-1) 2 * div (q-1) 2))



jacobiTest = describe "Jacobi symbol" $
  context "have" $ do
    it "(7/9907)=-1" $
      jacobi 7 9907 `shouldBe` -1
    it "(11/9907)=1" $
      jacobi 11 9907 `shouldBe` 1
    it "(13/9907)=1" $
      jacobi 13 9907 `shouldBe` 1
    it "(19/45)=1" $
      jacobi 19 45 `shouldBe` 1
    prop "with zero and a>=0" $
      \a -> let
          j = jacobi (abs a + 2) 0
        in j `shouldBe` 0
    prop "if p is prime is equivalente to legendre" $
      \a -> do
        let a' = abs a
        p <- pick primes
        let j = jacobi a' p
        let l = legendre a' p
        let c = j==l
        unless c $ error "no se"
    prop "(a/b) in {-1,0,1}" $
      \a b -> let
          b' = abs b
          j = jacobi a b'
        in (j==0 || j==1 || j==(-1))
    prop "If a = b  (mod p), then (a/p) = (b/p)" $
      \a b m -> let
          m' = abs m + 3
        in if odd m'
          then (a /= b `mod` m') || (jacobi a m' == jacobi b m')
          else (a /= b `mod` (m'+1)) || (jacobi a (m'+1) == jacobi b (m'+1))
    prop "(ab/n)=(a/n)(b/n)" $
      \a b m -> let
          m' = abs m + 3
        in if odd m'
          then jacobi (a*b) m' == jacobi a m' * jacobi b m'
          else jacobi (a*b) (m'+1) == jacobi a (m'+1) * jacobi b (m'+1)
    prop "(a/nm)=(a/n)(a/m)" $
      \a m n -> let
          m' = abs m + 3
          n' = abs n + 3
        in not (odd m' && odd n') || jacobi a (m'*n') == jacobi a m' * jacobi a n'
    prop "(a/n)(n/a)=(-1)^(div (a'-1) 2 * div (n'-1) 2)" $
      \a n -> let
        a' = abs a
        n' = abs n
      in not (odd a' && odd n' && gcd a' n' == 1) || jacobi a' n' * jacobi n' a' == ((-1)^(div (a'-1) 2 * div (n'-1) 2))

main::IO ()
main = hspec $
  describe "Math.Integer.hs" $ do
    legendreTest
    jacobiTest
